Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris>mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 205
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.001 sec)

MariaDB [(none)]> USE blog_db;
Database changed

MariaDB [blog_db]> CREATE TABLE users (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> email VARCHAR(100) NOT NULL,
    -> password VARCHAR(300) NOT NULL,
    -> datetime_created DATETIME NOT NULL,
    -> PRIMARY KEY (id)
    -> );
Query OK, 0 rows affected (0.364 sec)

Microsoft Windows [Version 10.0.14393]
(c) 2016 Microsoft Corporation. All rights reserved.

C:\Users\chris>mysql -h localhost -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.4.17-MariaDB mariadb.org binary distribution

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| blog_db            |
| information_schema |
| music_db           |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
+--------------------+
7 rows in set (0.080 sec)

MariaDB [(none)]> USE blog_db;
Database changed

MariaDB [blog_db]> SHOW TABLES
    -> ;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| users             |
+-------------------+
1 row in set (0.016 sec)

MariaDB [blog_db]> CREATE TABLE author (
    -> id INT NOT NULL AUTO_INCREMENT,
    -> name VARCHAR(50) NOT NULL,
    -> PRIMARY KEY (id)
    -> );
Query OK, 0 rows affected (0.316 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| author            |
| users             |
+-------------------+
2 rows in set (0.001 sec)

MariaDB [blog_db]> CREATE TABLE posts (
    ->     id INT NOT NULL AUTO_INCREMENT,
    ->     author_id INT NOT NULL,
    ->     title VARCHAR(500) NOT NULL,
    ->     content VARCHAR(5000) NOT NULL,
    ->     datetime_posted DATETIME NOT NULL,
    ->     PRIMARY KEY (id),
    ->     CONSTRAINT fk_posts_author_id
    ->     FOREIGN KEY (author_id)
    ->     REFERENCES author (id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    ->     );
Query OK, 0 rows affected (0.197 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| author            |
| posts             |
| users             |
+-------------------+
3 rows in set (0.000 sec)

MariaDB [blog_db]> CREATE TABLE post_comments (
    ->     id INT NOT NULL AUTO_INCREMENT,
    ->     post_id INT NOT NULL,
    ->     user_id INT NOT NULL,
    ->     PRIMARY KEY (id),
    ->     CONSTRAINT fk_post_comments_post_id
    ->     FOREIGN KEY (post_id)
    ->     REFERENCES posts(id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    ->     );
Query OK, 0 rows affected (0.380 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| author            |
| post_comments     |
| posts             |
| users             |
+-------------------+
4 rows in set (0.001 sec)

MariaDB [blog_db]> CREATE TABLE post_likes (
    ->     id INT NOT NULL AUTO_INCREMENT,
    ->     post_id INT NOT NULL,
    ->     user_id INT NOT NULL,
    ->     PRIMARY KEY (id),
    ->     CONSTRAINT fk_post_likes_post_id
    ->     FOREIGN KEY (user_id)
    ->     REFERENCES users (id)
    ->     ON UPDATE CASCADE
    ->     ON DELETE RESTRICT
    ->     );
Query OK, 0 rows affected (0.210 sec)

MariaDB [blog_db]> SHOW TABLES;
+-------------------+
| Tables_in_blog_db |
+-------------------+
| author            |
| post_comments     |
| post_likes        |
| posts             |
| users             |
+-------------------+
5 rows in set (0.001 sec)

MariaDB [blog_db]>
